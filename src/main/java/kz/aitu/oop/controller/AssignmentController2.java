package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import static java.lang.StrictMath.round;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        int count = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        int average = 0;
        for(Student student: studentFileRepository.getStudents()){
            count++;
        }
        average = count / 2;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average;
        int count = 0;
        double total = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for(Student student: studentFileRepository.getStudents()){
            total += student.getPoint();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        int count = 0;
        double total = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for(Student student: studentFileRepository.getStudents()){
            total += student.getAge();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        ArrayList<Double> arr = new ArrayList<>();
        int count = 0;
        for(Student student: studentFileRepository.getStudents()){
            arr.add(student.getPoint());
        }
        Collections.sort(arr);
        maxPoint = arr.get(arr.size()-1);

        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        ArrayList<Double> arr = new ArrayList<>();
        int count = 0;
        for(Student student: studentFileRepository.getStudents()){
            arr.add((double) student.getAge());
        }
        Collections.sort(arr);
        maxAge = arr.get(arr.size()-1);

        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        Hashtable<String, List<Student>> groups = new Hashtable<>();
        for(Student students: studentFileRepository.getStudents()){
            if(!groups.containsKey(students.getGroup()));
            {
                groups.put(students.getGroup(), new ArrayList<>());
            }
            groups.get(students.getGroup()).add(students);
        }
        double max_group_point = 0;
        for(List<Student> students: groups.values()){
            double total = 0;
            for(Student student: students){
                total+=student.getPoint();
            }
            double avg = total / students.size();
            if(avg > max_group_point){
                max_group_point = avg;
            }
            System.out.println(avg);
        }

        return ResponseEntity.ok(max_group_point);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        ArrayList<Double> arr = new ArrayList<>();
        int count = 0;
        for(Student student: studentFileRepository.getStudents()){
            arr.add((double) student.getAge());
            averageGroupAge += arr.get(count);
            count++;
        }
        averageGroupAge = averageGroupAge / count;

        return ResponseEntity.ok(round(averageGroupAge));
    }

//
    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
