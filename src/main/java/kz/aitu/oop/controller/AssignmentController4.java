package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public Object getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String result = "";
        for(Student students: studentFileRepository.getStudents()){
            if(students.getGroup().equals(group)){
                result+= students.getGroup()+" "+ students.getAge()+ " " + students.getName() + " " + students.getPoint() + "<br>";
            }
        }

        return ResponseEntity.ok(result);
    }

    /**
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        char a = 65;
        char s = '-';
        ArrayList<Double> obj = new ArrayList<>();
        StringBuilder result = new StringBuilder();
        int[] counter = new int[5];
        int count = 1;
        int j;
        for (Student student : studentFileRepository.getStudents()) {
            if(group.equals(student.getGroup())) {
                obj.add(student.getPoint());
            }
        }
        Collections.sort(obj);
        for(int i = 0; i < obj.size(); i++){
            if(obj.get(i) >= 95){
                j = 0;
                counter[j]+=count;
            }
            else if(obj.get(i) >= 85 &&obj.get(i) < 95){
                j = 1;
                counter[j]+=count;
            }
            else if(obj.get(i) >= 70 &&obj.get(i) < 85){
                j = 2;
                counter[j]+=count;
            }
            else if(obj.get(i) >= 55 && obj.get(i) < 70){
                j = 3;
                counter[j]+=count;
            }
            else{
                j = 4;
                counter[j]+=count;
            }
        }
        for(int i = 0; i < 5; i++){
            result.append(a).append(s).append(Integer.toString(counter[i])).append("<br>"); // appending A, then -, and count of grade
            if(i==3){
                a++;
            }
            a++;
        }

        return ResponseEntity.ok(result.toString());
    }

    /**
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        StringBuilder result = new StringBuilder();
        ArrayList<Double> obj = new ArrayList<>();
        for (Student student : studentFileRepository.getStudents()) {
            obj.add(student.getPoint());
        }
        Collections.sort(obj);
        for (int i = obj.size() - 1; i >= obj.size() - 5; --i) {// prints the top 5 students after sort;
            {
                for (Student student : studentFileRepository.getStudents()) {
                    if(obj.get(i).equals(student.getPoint())) {
                        result.append(student.getName()).append("  ").append(Double.toString(obj.get(i))).append("<br>");
                    }
                }
            }
        }
        return ResponseEntity.ok(result.toString());
    }
}
