package kz.aitu.oop.examples.assignment5;

public class Shape {
    private String color;
    private boolean filled;
    public Shape(){
        this.color = "green";
        this.filled = true;
    }
    public Shape(String given_color, boolean given_filter){
        color = given_color;
        filled = given_filter;
    }
    public void setColor(String s){
        color = s;
    }
    public String getColor(){
        return color;
    }
    public void setFilled(boolean a){
        filled = a;
    }
    public boolean getFilled(){
        return filled;
    }
    @Override
    public String toString(){
        if(filled){
            return "A Shape with color of " + color + " and filled";
        }
        else{
            return "A Shape with color of " + color + " and not filled";
        }
    }
}
