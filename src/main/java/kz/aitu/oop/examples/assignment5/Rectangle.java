package kz.aitu.oop.examples.assignment5;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(){
        this.width = width;
        this.length = length;
    }

    public Rectangle(double w, double l){
        width = w;
        length = l;
    }

    public Rectangle(double w, double l, String col, boolean filled){
        super(col, filled);
        width = w;
        length = l;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimeter(){
        return 2*(width+length);
    }

    @Override

    public String toString(){
        return "A Rectangle with width = " +
                width +  " and length = " + length +
                " ,which is a subclass of a " + super.toString();
    }
}
