package kz.aitu.oop.examples.assignment5;

public class Circle extends Shape{
    private double radius;
    double pi = 3.14;
    public Circle(){
        this.radius = radius;
    }

    public Circle(double radi){
        radius = radi;
    }

    public Circle(double radi, String color, boolean filled){
        super(color,filled);
        radius = radi;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        return pi*radius*radius;
    }

    public double getPerimeter(){
        return 2*pi*radius;
    }

    @Override
    public String toString(){
        return "A Circle with radius = " +
                radius + " which is a subclass of " +
                super.toString() + ", where ";
    }
}
