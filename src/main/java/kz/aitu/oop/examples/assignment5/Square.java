package kz.aitu.oop.examples.assignment5;

public class Square extends Rectangle{
    public Square(double side){
        super(side,side);
    }

    public Square(double side, String color, boolean filled){
       super(side,side,color,filled);
    }

    public double getSide(){
        return super.getWidth();
    }

    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString(){
        return "A square with side = " + super.getLength() +
                " which is a subclass of " + super.toString();
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    public static void main(String[] args) {
        Square s = new Square(6.0, "yellow", true);
        System.out.println(s.getArea()); // We don't need to change them
        System.out.println(s.getPerimeter());
    }
}
