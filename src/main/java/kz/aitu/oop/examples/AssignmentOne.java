package kz.aitu.oop.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

//Note:
//1)In order to print the values of averageLength Arrays.toString("averageLength") should be called;

public class AssignmentOne {
    public static void main(String[] args) throws Exception {
        String l = AssignmentOne.getFiles();
        ArrayList<Double> arr = new ArrayList<>();
        for(int i = 0; i < l.length(); i++){
            if(l.charAt(i) == '-'){//If we found a - sign in file we need to do some extra if's
                char x = l.charAt(i+1);
                double y = -(x - '0');//Direct parsing unavailable cause char is just a number and I used ASCII table division with '0'
                arr.add(y);
                i++;
            }
            else {
                char x = l.charAt(i);
                double y = x - '0';
                arr.add(y);
            }

        }
        Shape shape = new Shape(arr);
        System.out.println("Perimeter:");
        System.out.printf("%.1f",shape.calculatePerimeter());
        System.out.println();
        System.out.println("LongestSide:");
        System.out.printf("%.1f",shape.LongestSide());
        System.out.println();
        System.out.println("AverageLength of each side:");
        System.out.println();
        for(int i = 0; i < shape.averageLength().length; i++){
            System.out.printf("%.1f",shape.averageLength()[i]);
            System.out.println();
        }
    }

    public static String getFiles() throws IOException {
        File file = new File("/Users/harmonyof/Desktop/index.txt");
        Scanner sc = new Scanner(file);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String str = new String(data, StandardCharsets.UTF_8);
        str = str.replaceAll("[\", $\"+\n]", "");
        return str;
    }
}
