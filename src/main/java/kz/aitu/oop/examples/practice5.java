package kz.aitu.oop.examples;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
public class practice5 {

    public static boolean fileExist(String k){
        int detection_of_Exception = 0;
        try{
            System.out.println(k);
        }
        catch(NullPointerException val){
            detection_of_Exception = 1;
        }
        return detection_of_Exception != 1;
    }
    public static boolean isInt(String string){
        int detection_of_Exception = 0;
        try{
            int i = Integer.parseInt(string);
        } catch(NullPointerException | NumberFormatException e){
            e.printStackTrace();
            detection_of_Exception = 1;
        }
        return detection_of_Exception != 1;
    }
    public static boolean isDouble(String string){
        int detection_of_Exception = 0;
        try{
            double i = Double.parseDouble(string);
        } catch(NullPointerException | NumberFormatException e){
            e.printStackTrace();
            detection_of_Exception = 1;
        }
        return detection_of_Exception != 1;
    }
    public static void main(String[] args){
        String path = "/Users/harmonyof/Desktop/Student.rtf";
        String checker = "";
        //fileExist method to try to find a file if it exists and if not
        try {
            checker = Files.readString(Paths.get(path));
        }
        catch(IOException e){
            e.printStackTrace();
        }
        if(fileExist(checker)){
            System.out.println("Yes we found our file");
        }
        else{
            System.out.println("File not Found!");
        }
        /////////////////////////////////////////////////////////////////
        // Finding an exception for the Integer parsing
        String number = "12345";
        if(!isInt(number)){
            System.out.println("Number cannot be converted to a Integer");
        } else{
            int p = Integer.parseInt(number);
            System.out.println("Your value: " + p);
        }
        /////////////////////////////////////////////////////////////////
        // Finding the real number from a string block
        String num = "3242340.22342";
        if(!isDouble(num)){
            System.out.println("Number cannot be converted to a Double");
        } else{
            double s = Double.parseDouble(num);
            System.out.println("Your value: " + s);
        }
        /////////////////////////////////////////////////////////////////
        try{
            String s = null;
            System.out.println(s.length());
        }
        catch (NullPointerException k){
            k.printStackTrace();
        }
        System.out.println("Exception above found, but we need to continue our journey ))");
    }
}
