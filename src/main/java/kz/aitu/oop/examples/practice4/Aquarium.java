package kz.aitu.oop.examples.practice4;

import java.util.HashMap;
import java.util.Map;

public class Aquarium{
    // Accessories that you need to implement
    private HashMap<Integer, String> CostAndAccessories = new HashMap<>();
    //Reptiles-name and the number of that reptiles in aquarium
    protected HashMap<String, Integer> TypeOfReptiles = new HashMap<>();
    //Amphibians-name and the number of that amphibians in aquarium
    protected HashMap<String, Integer> TypeOfFish = new HashMap<>();
    String[] types_of_fishes = {"Cyprinidae","Anabonteidei","Catfish","Charcoids","Cichlids","Loaches","Rainbowfish","Guppy"};
    String[] types_of_reptiles = {"turtle","lizard","snake","crocodile","aligator","frog","toad","salamander"};
    protected double Volume;
    protected boolean check = false;
    // Here 0.027 standard size of aquarium

    public void setVolume(double volume) {
        Volume = volume;
    }

    public double getVolume() {
        return Volume;
    }

    public HashMap<Integer, String> getCostAndAccessories() {
        return CostAndAccessories;
    }
    public void addNecessities(){
        CostAndAccessories.put(10000,"Sand and Gravel");
        CostAndAccessories.put(8000, "Fish Nets");
        CostAndAccessories.put(12000,"Cleaning Tools");
        CostAndAccessories.put(44000, "Air pipe and Air Stones");
        CostAndAccessories.put(18000,"Aquarium Plants");
        CostAndAccessories.put(15000,"Condensation Covers");
        CostAndAccessories.put(700,"Thermometer");
        //for each 1 m^3 water 40000 kzt
        CostAndAccessories.put((int) (Volume*500000),"Price Aquarium");
    }
    public void AddAccessories(int price, String AccessoriesName){
        CostAndAccessories.put(price,AccessoriesName);
    }

    public HashMap<String, Integer> getTypeOfReptiles() {
        return TypeOfReptiles;
    }

    public HashMap<String, Integer> getTypeOfFish() {
        return TypeOfFish;
    }

    public void AddReptiles(String NameOfReptile, int NumberOfReptiles){
            for (int i = 0; i < types_of_reptiles.length; i++) {
                if (NameOfReptile.equals(types_of_reptiles[i])) {
                    TypeOfReptiles.put(NameOfReptile, NumberOfReptiles);
                    check = true;
                }
            }
    }

    public void AddFish(String NameOfFishes, int NumberOfFishes){
        for(int i = 0; i < types_of_fishes.length; i++){
            if(NameOfFishes.equals(types_of_fishes[i])){
                TypeOfFish.put(NameOfFishes,NumberOfFishes);
            }
        }
    }

    public void GetAccessoriesPrice(){
        int sum = 0;
        for(Map.Entry<Integer, String> entry : CostAndAccessories.entrySet()){
            sum += entry.getKey();
        }
        System.out.println("Total sum of " + getVolume() + " m^3 aquarium is: " + sum + " kzt");
    }

    public void swim(){
    }
    public void lives(){

    }
}
