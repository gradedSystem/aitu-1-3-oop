package kz.aitu.oop.examples.practice4;

import java.util.Map;

public class Fishes extends Aquarium{

    @Override
    public void swim() {
        if(getVolume() >= 10 || !check){
            for(Map.Entry<String, Integer> entry : getTypeOfFish().entrySet()){
                System.out.println(entry.getKey() + " is in danger.");
            }
        } else{
            System.out.println("There are no reptiles, so fishes can swim safely.");
        }
    }

    @Override
    public void AddFish(String NameOfFishes, int NumberOfFishes) {
        super.AddFish(NameOfFishes, NumberOfFishes);
    }

    public void lives(){
        int sum = 0;
            for(Map.Entry<String, Integer> entry: getTypeOfFish().entrySet()){
                sum += entry.getValue();
            }
            System.out.println("There are " + sum + " Fishes.");
        }
}
