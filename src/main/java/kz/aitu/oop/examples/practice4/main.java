package kz.aitu.oop.examples.practice4;

public class main {
    public static void main(String[] args) {
        Aquarium aquarium = new Aquarium();
        // here value of 1 unit is 1 cm
        aquarium.setVolume(11);
        //Whenever you create aquarium you should call function addNecessities in order to make sure that you will give best live for you fish
        aquarium.addNecessities();
        Fishes fishes = new Fishes();
        Reptiles reptiles = new Reptiles();
        fishes.AddFish("Cyprinidae", 5);
        fishes.AddFish("Charcoids", 5);
        fishes.AddFish("Rainbowfish", 5);

        reptiles.AddReptiles("lizard", 1);


        reptiles.lives();
        reptiles.swim();
        // If the volume of the is more than 10 m^3 then reptiles can fit into aquarium, then fishes certainly in danger
        fishes.swim();
        fishes.lives();
        aquarium.GetAccessoriesPrice();

        /* Results of the above equations
            There are 1 Reptiles.
            Reptiles swimming and it's very dangerous to be here for fish
            Charcoids is in danger.
            Cyprinidae is in danger.
            Rainbowfish is in danger.
            There are 15 Fishes.
            Total sum of 11.0 m^3 aquarium is: 5607700 kzt
        */
    }
}
