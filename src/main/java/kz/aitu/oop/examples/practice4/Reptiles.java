package kz.aitu.oop.examples.practice4;

import java.util.ArrayList;
import java.util.Map;

public class Reptiles extends Aquarium{
    //Even though some of the listed names are not Reptiles, they are more Amphibians, just for the sake of broader list :)

    @Override
    public void swim() {
        String msg = (check) ? "Reptiles swimming and it's very dangerous to be here for fish" : "There are no reptiles";
        System.out.println(msg);
    }

    @Override
    public void AddReptiles(String NameOfReptile, int NumberOfReptiles) {
        super.AddReptiles(NameOfReptile,NumberOfReptiles);
    }
    public void lives(){
        int sum = 0;
            for(Map.Entry<String, Integer> entry: super.getTypeOfReptiles().entrySet()){
                sum += entry.getValue();
            }
            if(sum == 0){
                check = true;
            } else{
                System.out.println("There are " + sum + " Reptiles.");
            }
    }
}
