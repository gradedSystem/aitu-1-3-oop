package kz.aitu.oop.examples;

public class Point {
    private double x;
    private double y;
    public Point(){

    }
    public double calculateDistance(Point a){
        return Math.sqrt(Math.pow(a.getX()- this.getX(),2) + Math.pow(a.getY()- this.getY(),2));
    }
    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    public double getX(){
        return x;
    }
    public void setX(){
        this.x = x;
    }
    public double getY(){
        return y;
    }
    public void setY(){
        this.y = y;
    }

    public static void main(String[] args) {
        Point a = new Point(3,5);
        Point b = new Point(5,8);
        Point c = new Point(7,2);
        System.out.println(a.calculateDistance(b));
        System.out.println(b.calculateDistance(c));
        System.out.println(a.calculateDistance(c));
    }
}
