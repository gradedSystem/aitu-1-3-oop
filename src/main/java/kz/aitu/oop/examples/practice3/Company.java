package kz.aitu.oop.examples.practice3;

import org.springframework.http.converter.json.GsonBuilderUtils;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;

public class Company {
    private String CEO;
    private HashMap<Integer,String> Project = new HashMap<>();
    final int CEO_wage = 500000;
    final int Wage_of_Marketing_Manager = 300000;
    final int Manager_production_wage = 350000;
    final int SalesPerson_wage = 180000;
    final int Workers_wage = 150000;
    final double Project_bonus_managers = 0.2;
    final double Project_bonus_workers = 0.15;
    protected ArrayList<String> SalesPerson = new ArrayList<>();
    protected ArrayList<String> Workers = new ArrayList<>();
    private int Budget;

    public void setCEO(String CEO) {
        this.CEO = CEO;
    }

    public ArrayList<String> getWorkers() {
        return Workers;
    }

    public ArrayList<String> getSalesPerson() {
        return SalesPerson;
    }

    public void setBudget(int budget) {
        Budget = budget;
    }

    public HashMap<Integer, String> getProjects() {
        return Project;
    }

    public int Number_of_projects() {
        return Project.size();
    }

    public int Number_Of_SalesPersons(ArrayList<String> salesPerson){
        return salesPerson.size();
    }

    public int Number_Of_Workers(ArrayList<String> Workers){
        return Workers.size();
    }
    public void addWorker(String s){
        Workers.add(s);
    }
    public void addSalesPerson(String s){
        SalesPerson.add(s);
    }

    public void addProjects(int ID, String k){
        Project.put(ID, k);
    }

    // Estimate total cost of the all workers, CEO's and managers cost on one project
    public void EstimateCost(){
        double TotalCost_Tier1 = (CEO_wage+Manager_production_wage+Wage_of_Marketing_Manager)*(1+Project_bonus_managers);
        double TotalCost_Tier2 = (Number_Of_SalesPersons(SalesPerson)*SalesPerson_wage + Number_Of_Workers(Workers)*Workers_wage)*(1+Project_bonus_workers);
        int Num_of_proj = Number_of_projects();
        double Total = (TotalCost_Tier1+TotalCost_Tier2)*Num_of_proj;
        String msg = (Total > Budget) ? "Exceeds the limit of the Budget" : "Within the range of the budget: "+Total;
        System.out.println(msg);
    }


}
