package kz.aitu.oop.examples.practice3;

import java.util.ArrayList;

public class Manager_production extends Company{
    private String Manager;

    public void setManager(String manager) {
        Manager = manager;
    }

    public String getManager() {
        return Manager;
    }

    @Override
    public int Number_Of_Workers(ArrayList<String> Workers) {
        return super.Number_Of_Workers(Workers);
    }
    //Here we can find the cost on production sector
    @Override
    public void EstimateCost() {
        System.out.println((Workers_wage*Number_Of_SalesPersons(Workers)*(1+Project_bonus_workers) + Manager_production_wage*(1+Project_bonus_managers)));
    }
}
