package kz.aitu.oop.examples.practice3;

import java.util.ArrayList;

public class Manager_marketing extends Company{
    private String Manager;

    public void setManager(String manager) {
        Manager = manager;
    }

    public String getManager() {
        return Manager;
    }

    @Override
    public int Number_Of_SalesPersons(ArrayList<String> salesPerson) {
        return super.Number_Of_SalesPersons(salesPerson);
    }
    //Here we can find the cost on marketin sector
    @Override
    public void EstimateCost() {
        System.out.println((SalesPerson_wage*Number_Of_SalesPersons(SalesPerson)*(1+Project_bonus_workers) + Wage_of_Marketing_Manager*(1+Project_bonus_managers)));
    }
}
