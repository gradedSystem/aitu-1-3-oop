package kz.aitu.oop.examples.practice3;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        Manager_production manager_production = new Manager_production();
        Manager_marketing manager_marketing = new Manager_marketing();
        company.addWorker("Levi");
        company.addWorker("Mess");
        company.addWorker("Uri");
        company.addWorker("Ursula");
        company.addWorker("Fonderline");
        company.addWorker("Trump");

        company.addSalesPerson("David");
        company.addSalesPerson("Suriel");
        company.addSalesPerson("Jacquez");
        company.addSalesPerson("Hero");
        company.addSalesPerson("Nevr");

        manager_marketing.setManager("Hasan");
        manager_production.setManager("Ezequil");

        manager_marketing.EstimateCost();
        manager_production.EstimateCost();
        //If the number on one project exceeds the budget, therefore we need to cut the workers or people who will work on this specific project
        company.setCEO("Yedige");
        company.setBudget(5000000);
        company.addProjects(23432, "Project1");
        company.EstimateCost();
    }
}
