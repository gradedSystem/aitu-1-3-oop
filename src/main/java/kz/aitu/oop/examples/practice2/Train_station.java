package kz.aitu.oop.examples.practice2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Train_station{
    private String station_name;
    private String Destination_time;
    //Station names is set to static and they should be interconnected with each other
    // For the sake of example I used Zurich~Switzerland's railway station https://en.wikipedia.org/wiki/Z%C3%BCrich_Hauptbahnhof
    // I did not used all stations and included only some of them
    // Here Integer used to store the destination time to certain station, for example Aarau - 17:30 destination time
    // Therefore Aarau->Bern takes 2 hours, consequently Bern - 19:30 and so on.
    private static ArrayList<String> Retrieve_data() throws IOException {
        ArrayList<String> Station_names = new ArrayList<>();
        File file = new File("/Users/harmonyof/Desktop/Student.txt");
        Scanner sc = new Scanner(file);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String str = new String(data, StandardCharsets.UTF_8);
        String update = "";
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == ','){
                Station_names.add(update);
                update = "";
            } else{
                update += str.charAt(i);
            }
        }
        return Station_names;
    }

    //This function directly shows all train stations
    public ArrayList<String> getStation_names() throws IOException {
        return Retrieve_data();
    }

}
