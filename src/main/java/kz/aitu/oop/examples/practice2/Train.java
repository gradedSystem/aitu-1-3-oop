package kz.aitu.oop.examples.practice2;

import java.io.IOException;
import java.util.HashMap;

public class Train extends Passenger{
    final int Num_of_seats = 100;
    int[] seats = new int[Num_of_seats];
    int Number_of_Passengers;
    private Passenger passenger;

    @Override
    public void add(String passenger_Name, String passenger_Surname, int Passenger_id) throws IOException {
        super.add(passenger_Name, passenger_Surname, Passenger_id);
        Number_of_Passengers++;
    }

    // We can calculate the number of passengers every time we insert new passenger
    public void NumberOfPassengers(){
        System.out.println(Number_of_Passengers);
    }

    public void available_seats(){
        System.out.println(Number_of_Passengers);
        for(int i = 0; i < Number_of_Passengers; i++){
            seats[i] = i+1;
        }
        System.out.println(Num_of_seats - Number_of_Passengers);
    }

    public static void main(String[] args) throws IOException {
        Train passenger = new Train();
        passenger.add("Yedige", "Ashmet", 234234);
        passenger.add("asfasf", "asfasf", 234324);
        passenger.NumberOfPassengers();
        passenger.available_seats();
    }

}