package kz.aitu.oop.examples.practice2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
// Sort of online or cashier hand-to-hand ticket purchase
public class Passenger extends Train_station{
    private String Passenger_Name;
    private String Passenger_Surname;
    private int Passenger_id;
    String destination;
    int update = 0;
    private final HashMap<Integer, String> Information_about_passenger = new HashMap<>();

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setPassenger_Surname(String passenger_Surname) {
        Passenger_Surname = passenger_Surname;
    }

    public void setPassenger_Name(String passenger_Name) {
        Passenger_Name = passenger_Name;
    }

    public void setPassenger_id(int passenger_id) {
        Passenger_id = passenger_id;
    }

    public void add(String passenger_Name, String passenger_Surname, int Passenger_id) throws IOException {
        setPassenger_Name(passenger_Name);
        setPassenger_Surname(passenger_Surname);
        setPassenger_id(Passenger_id);
        buyTicket();
    }

    public HashMap<Integer, String> getInformation_about_passenger(){
        return Information_about_passenger;
    }

    private boolean contains(String s, String k){
        s = s.replaceAll("[^a-zA-Z]", "");
        int count = 0;
        for(int i = 0; i < s.length(); i++)
            for(int j = i; j < k.length(); j++)
                if(s.charAt(i) == k.charAt(j) && s.charAt(i) != '-')
                {
                    count++;
                    break;
                }
        return count == k.length();
    }
    // Here passenger buys ticket from some train station
    private void buyTicket() throws IOException {
        System.out.println("Please choose where you want to go");
        whereTo(Passenger_Name,Passenger_Surname, Passenger_id);
        // check the possible train station name if there none print type again, if he founds you should also choose
        // from which train station to which destination passenger should go to
    }

    private void whereTo(String Passenger_Name,String Passenger_Surname, int Passenger_id) throws IOException {
        Train_station train_station = new Train_station();
        ArrayList<String> chekcer = train_station.getStation_names();
        Scanner sc = new Scanner(System.in);
        String destination = "";
        while(true){
            //Generally there are no more than 100 stations, and it won't be mistake if we use O(n^3) since the value is not huge
            destination = sc.next();
            int k = 0;
            for (String s : chekcer) {
                if (contains(s, destination)) {
                    k++;
                }
            }
            if(k == 0)
                destination = "";
            if (!destination.isEmpty()) {
                String Info = Passenger_Name + " " + Passenger_Surname + " " + Passenger_id + " " + destination;
                update += 1;
                try{
                    Information_about_passenger.put(update, Info);
                } catch(NullPointerException e){
                    e.printStackTrace();
                }
                System.out.println("Ticket bought");
                break;
            }
            System.out.println("Insert Again");
        }
    }
}
