package kz.aitu.oop.examples.practice5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class Stones {
    private String Type = "";
    private LinkedHashSet<String> necklace = new LinkedHashSet<>();
    private int weight;
    // Quantifier used in order to multiply each carats and the price for each carat
    final int Semi_quantifier = 10000;
    final int Precious_quantifier = 50000;
    private int price = 0;
    protected static HashMap<String, Integer> semi_precious = new HashMap<>() {
        {
            put("turquoise",5);
            put("aquamarine",7);
            put("moonStone", 4);
            put("peridot", 8);
            put("roseQuartz", 9);
            put("zircon", 3);
            put("opal", 6);
        }
        ;
    };

    protected static HashMap<String, Integer> precious = new HashMap<>() {
        {
            put("diamond",20);
            put("ruby",19);
            put("blueSapphire", 24);
            put("emerald", 18);
        }
        ;
    };


    public void AddStonesToNecklace(String type, ArrayList<String> str){
        // checking for spell errors if any exists
        type = type.replaceAll("[-+.^:,]","");
        type = type.toLowerCase();
        if(type.equals("semiprecious")){
            for(String s : str) {
                for (Map.Entry<String, Integer> entry : semi_precious.entrySet()) {
                    if(entry.getKey().equals(s)){
                        necklace.add(s);
                    }
                }
            }
        } else if(type.equals("precious")){
            for(String s : str) {
                for (Map.Entry<String, Integer> entry : precious.entrySet()) {
                    if(entry.getKey().equals(s)){
                        necklace.add(s);
                    }
                }
            }
        } else{
            System.out.println("The type of stones are not yet discovered.");
        }
    }
    // we can get total price
    public void Price(){
        for(String str : necklace){
            for(Map.Entry<String,Integer> entry : semi_precious.entrySet()){
                if(entry.getKey().equals(str)){
                    price += entry.getValue()*Semi_quantifier;
                }
            }
            for(Map.Entry<String,Integer> entry : precious.entrySet()){
                if(entry.getKey().equals(str)){
                    price += entry.getValue()*Precious_quantifier;
                }
            }
        }
        System.out.println("The TotalSum of the necklace you made is:" + price + " kzt");
    }
}
