package kz.aitu.oop.examples.practice5;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        Stones stones = new Stones();
        Semi_Precious semi_precious = new Semi_Precious();
        Precious precious = new Precious();
        ArrayList<String> necklace = new ArrayList<>();
        necklace.add("diamond");
        necklace.add("blueSapphire");
        necklace.add("turquoise");
        necklace.add("aquamarine");
        stones.AddStonesToNecklace("semiprecious",necklace);
        stones.AddStonesToNecklace("precious",necklace);
        stones.Price();
        System.out.println(semi_precious.getDescription(necklace));
        System.out.println(precious.getDescription(necklace));
        semi_precious.SemiPreciousStonePrice(necklace);
        precious.PreciousStonePrice(necklace);
    }
}
