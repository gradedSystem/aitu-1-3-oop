package kz.aitu.oop.examples.practice5;

import java.util.ArrayList;
import java.util.Map;

public class Semi_Precious extends Stones {
    private String description = "";
    private int sum = 0;

    //Description of the stone that Customer made
    public String getDescription(ArrayList<String> necklace) {
        int i = 1;
        for(String str : necklace){
            for(Map.Entry<String, Integer> entry : semi_precious.entrySet()){
                if(entry.getKey().equals(str)){
                    description += i + ".";
                    description += entry.getKey();
                    description += " and the weight is:";
                    description += entry.getValue() + "\n";
                    i++;
                }
            }
        }
        return description;
    }

    public void SemiPreciousStonePrice(ArrayList<String> necklace){
        for(String str : necklace){
            for(Map.Entry<String, Integer> entry : semi_precious.entrySet()){
                if(entry.getKey().equals(str)){
                    sum += entry.getValue()*Semi_quantifier;
                }
            }
        }
        System.out.println("The total value of Semi Precious Stones are: " + sum);
    }
}
