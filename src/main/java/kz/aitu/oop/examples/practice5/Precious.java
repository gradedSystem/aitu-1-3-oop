package kz.aitu.oop.examples.practice5;

import java.util.ArrayList;
import java.util.Map;

public class Precious extends Stones{
    private String description = "";
    private int sum = 0;
    public String getDescription(ArrayList<String> necklace) {
        int i = 1;
        for(String str : necklace){
            for(Map.Entry<String, Integer> entry : precious.entrySet()){
                if(entry.getKey().equals(str)){
                    description += i + ".";
                    description += entry.getKey();
                    description += " and the weight is:";
                    description += entry.getValue() + "\n";
                    i++;
                }
            }
        }
        return description;
    }

    public void PreciousStonePrice(ArrayList<String> necklace){
        for(String str : necklace){
            for(Map.Entry<String, Integer> entry : semi_precious.entrySet()){
                if(entry.getKey().equals(str)){
                    sum += entry.getValue()*Precious_quantifier;
                }
            }
        }
        System.out.println("The total value of Precious Stones are: " + sum);
    }
}
