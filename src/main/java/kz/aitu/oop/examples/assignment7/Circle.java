package kz.aitu.oop.examples.assignment7;

public class Circle extends Shape{
    protected double radius;
    private double pi = 3.14;
    public Circle() {
        this.radius = radius;
    }

    public Circle(double radi){
        radius = radi;
    }
    public Circle(double radi, String col, boolean fil){
        super(col,fil);radius=radi;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    @Override
    public double getArea(){
        return pi*radius*radius;
    }

    @Override
    public double getPerimeter() {
        return 2*pi*radius;
    }

    @Override
    public String toString(){
        return "Color["+super.toString()+",radius="+radius+"]";
    }
}
