package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangle{
    public Square(){
        this.width = width;
        this.length = length;
    }

    public Square(double side){
        width = side;
        length = side;
    }

    public Square(double side, String col, boolean fil){
        super(side,side,col,fil);
    }

    public double getSide(){
        return length;
    }
    public void setSide(double side){
        width = side;
        length = side;
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString(){
        return "Shape["+super.toString()+"]";
    }
}
