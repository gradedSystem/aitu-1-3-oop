package kz.aitu.oop.examples.assignment7.Subtask2;

public interface Movable {
    public void moveUp();
    public void moveDown();
    public void moveRight();
    public void moveLeft();

    class MovablePoint implements Movable{
        private int x;
        private int y;
        private int xSpeed;
        private int ySpeed;

        public MovablePoint(int x, int y, int xSpeed, int ySpeed){
            this.x = x;
            this.y = y;
            this.xSpeed = xSpeed;
            this.ySpeed = ySpeed;
        }

        @Override
        public String toString(){
            return "X = "+ x + ", Y = " + y + "The speed for X = " + xSpeed + ", and for Y = " + ySpeed;
        }

        @Override
        public void moveUp() {

        }

        @Override
        public void moveDown() {

        }

        @Override
        public void moveRight() {

        }

        @Override
        public void moveLeft() {

        }
    }

    class MovableCircle implements Movable{
        private int radius;
        private MovablePoint center;

        public MovableCircle(int x, int y, int xSpeed, int ySpeed){
            this.center = new MovablePoint(x,y,xSpeed,ySpeed);
        }

        @Override
        public String toString(){
            return center.toString();
        }

        @Override
        public void moveUp() {

        }

        @Override
        public void moveDown() {

        }

        @Override
        public void moveRight() {

        }

        @Override
        public void moveLeft() {

        }
    }
}
