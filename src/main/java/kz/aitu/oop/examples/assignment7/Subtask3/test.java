package kz.aitu.oop.examples.assignment7.Subtask3;

public class test {
    public static void main(String[] args) {
        GeometricObjects.ResizableCircle s = new GeometricObjects.ResizableCircle(8);
        System.out.println(s.toString());
        System.out.println(s.getPerimeter());
        System.out.println(s.getArea());
        s.resize(80);
        System.out.println(s.getPerimeter());
        System.out.println(s.getArea());
    }
}
