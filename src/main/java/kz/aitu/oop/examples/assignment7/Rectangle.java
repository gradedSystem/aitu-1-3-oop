package kz.aitu.oop.examples.assignment7;

public class Rectangle extends Shape {
    protected double width;
    protected  double length;

    public Rectangle(){
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length,String col, boolean fil){
        super(col, fil); this.width = width;  this.length = length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return width*length;
    }

    @Override
    public double getPerimeter() {
        return 2*(width+length);
    }

    @Override
    public String toString(){
        return "Rectangle[Shape"+super.toString()+",width="+width+",length="+length+"]";
    }
}
