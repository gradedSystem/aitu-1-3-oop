package kz.aitu.oop.examples.assignment7.Subtask3;

public interface GeometricObjects {
    public double getPerimeter();
    public double getArea();

    class Circle implements GeometricObjects{
        protected double radius;

        public Circle(double radius){
            this.radius = radius;
        }

        @Override
        public String toString(){
            return "Radius is = "+radius;
        }

        @Override
        public double getPerimeter() {
            return 2*3.14*radius;
        }

        @Override
        public double getArea() {
            return 3.14*radius*radius;
        }
    }

    public interface Resizable{
        public void resize(int percent);
    }

    class ResizableCircle extends Circle implements Resizable{

        public ResizableCircle(double radius) {
            super(radius);
        }

        @Override
        public String toString(){
            return super.toString();
        }

        @Override
        public void resize(int percent){
            radius = (radius*percent)/100;
        }

    }
}
