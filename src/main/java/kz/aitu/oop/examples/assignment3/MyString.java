package kz.aitu.oop.examples.assignment3;

import java.io.*;
import java.util.Scanner;

public class MyString {
    private int[] array;
    public MyString(int[] values){
        array = values;
    }
    public int length(){
        return array.length;
    }
    public int valueAt(int position){
        if(position > array.length){
            return -1;
        }
        else{
            return array[position];
        }
    }
    public boolean contains(int value){
        for (int item : array) {
            if (value == item) {
                return true;
            }
        }
        return false;
    }
    public int count(int value){
        int count = 0;
        for(int i : array){
            if(value == i){
                count++;
            }
        }
        return count;
    }
    public boolean equals(int[] arr, int[] arr2){
        if(arr.length == arr2.length){
            for(int i = 0; i < arr.length; i++){
                if(arr[i] != arr2[i]){
                    return false;
                }
            }
        }
        return true;
    }
    static void solve(boolean val){
        if(val){
            System.out.println("They are equal and in the same order");
        }
        else{
            System.out.println("No the values are not same");
        }
    }
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int[] writer = arr;
        FileReader fileReader = new FileReader();
        File tmpDir = new File("/Users/harmonyof/Desktop/Student.txt");
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(tmpDir)); // 2 task
        boolean exists = tmpDir.exists();
        if ((exists)) {
            outputStream.writeObject(arr);
            outputStream.close();
        } else {
            System.exit(0);
        }
        // 3rd task
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
                "/Users/harmonyof/Desktop/Student.txt"));
        arr = (int[]) (ois.readObject());
        MyString mys = new MyString(arr);
        solve(mys.equals(writer, arr));//checks whether the values are same in both written and read file;;
    }
}
