package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class MySpecialString {
    private int[] array;
    public MySpecialString(int[] values){
        for(int i = 0; i < values.length; i++){
            for(int j = i+1; j < values.length; i++){
                if(values[i] == values[j] && j < values.length - 2){
                    values[j] = values[j+1];
                }
            }
        }
        array = values;
    }
    public int length(){
        return array.length;
    }
    public int valueAt(int position){
        if(position > array.length){
            return -1;
        }
        else{
            return array[position];
        }
    }
    public boolean contains(int value){
        for (int item : array) {
            if (value == item) {
                return true;
            }
        }
        return false;
    }
    public int count(int value){
        int count = 0;
        for(int i : array){
            if(value == i){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        MySpecialString obj = new MySpecialString(arr);
    }
}
