package kz.aitu.oop.examples;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Shape {
    private ArrayList<Double> k;

    public Shape(ArrayList<Double> k){
        this.k = k;
    }
    public ArrayList<Double> getPoints(){
        return k;
    }

    public void addPoint(Point a){
        k.add(a.getX());
        k.add(a.getY());
    }

    public double calculatePerimeter(){
        ArrayList<Double> s = k;
        double perimeter = 0;
        int count = 0;
        for(int i = 0; i < s.size(); i++){
            if(s.size() >= 6){// To be perimeter it should be at least triangle
                if(count > s.size()-4){
                    count = 0;
                    Point a = new Point(s.get(count), s.get(count+1));
                    Point b = new Point(s.get(s.size()-2), s.get(s.size() - 1));
                    perimeter += a.calculateDistance(b);
                    break;
                }
                    Point a = new Point(s.get(count), s.get(count+1));
                    Point b = new Point(s.get(count+2), s.get(count+3));
                    perimeter += a.calculateDistance(b);
                    count += 2;
            }
        }
        return perimeter;
    }
    public double LongestSide(){
        ArrayList<Double> s = k;
        int count = 0, f = 0;
        double[] arr = new double[s.size()/2];
        for(int i = 0; i < s.size(); i++){
            if(count > s.size()-4){
                    count = 0;
                    Point a = new Point(s.get(count), s.get(count+1));
                    Point b = new Point(s.get(s.size()-2), s.get(s.size() - 1));
                    arr[f] = a.calculateDistance(b);
                    break;
                }
                Point a = new Point(s.get(count), s.get(count+1));
                Point b = new Point(s.get(count+2), s.get(count+3));
                arr[f] = a.calculateDistance(b);
                f++;
                count += 2;
        }
        double longest = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(arr[i] > longest){
                longest = arr[i];
            }
        }
        return longest;
    }
    public double[] averageLength(){
        double[] arr = new double[k.size()/2];
        int count = 0, f = 0;
        for(int i = 0; i < k.size(); i++){
            if(count > k.size()-4){ // This is for the first and last Points connection
                count = 0;
                Point a = new Point(k.get(count), k.get(count+1));
                Point b = new Point(k.get(k.size()-2), k.get(k.size() - 1));
                arr[f] = a.calculateDistance(b)/2;
                break;
            }
            Point a = new Point(k.get(count), k.get(count+1));
            Point b = new Point(k.get(count+2), k.get(count+3));
            arr[f] = a.calculateDistance(b)/2;
            f++;
            count += 2;
        }
        return arr;
    }
}
